README
Goal: Train sequence-to-sequence and sequence-labelling models to punctuate unpunctuated Indonesian text. 

This repository contains code to train, decode and evaluate a seq2seq transformer model (in ./seq2seq) 
and a seqlab CRF model (in ./seqlab). 

A trained seq2seq model is saved to the VM in the directory /mnt/data/data-gpu/advaith_workspace/punc_model.
